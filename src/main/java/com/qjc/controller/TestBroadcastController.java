package com.qjc.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qjc.entity.TConfig;
import com.qjc.service.ITConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: TestBroadcastController
 * @Description:
 * @Author: qjc
 * @Date: 2022/4/27 1:10 下午
 */
@RestController
@RequestMapping("/broadcast")
@Api(value = "/broadcast", tags = "测试广播表操作")
public class TestBroadcastController {

    @Autowired
    private ITConfigService configService;

    @GetMapping("/config/add")
    @ApiOperation(value = "插入配置信息", notes = "插入配置信息")
    public String testAdd(@ApiParam("备注") @RequestParam("remark") String remark) {
        TConfig config = new TConfig();
        config.setRemark(remark);
        configService.save(config);
        return "success";
    }

    @GetMapping("/config/query")
    @ApiOperation(value = "查询配置信息", notes = "查询配置信息")
    public TConfig testQuery(@ApiParam("备注") @RequestParam("remark") String remark) {
        TConfig one = configService.getOne(new LambdaQueryWrapper<TConfig>().eq(TConfig::getRemark, remark));
        return one;
    }
}
