package com.qjc.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qjc.entity.TCity;
import com.qjc.mapper.TCityMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName: TestReadWriteController
 * @Description:
 * @Author: qjc
 * @Date: 2022/4/27 1:15 下午
 */
@RestController
@RequestMapping("/ms")
@Api(value = "/ms", tags = "测试读写分离操作")
public class TestReadWriteController {

    @Resource
    private TCityMapper cityMapper;

    @GetMapping("/write")
    @ApiOperation(value = "插入城市信息", notes = "插入城市信息")
    public String testAdd(@ApiParam("省份") @RequestParam("province") String province,
                          @ApiParam("城市") @RequestParam("city") String city) {
        TCity tCity = new TCity();
        tCity.setName(city);
        tCity.setProvince(province);
        cityMapper.insert(tCity);
        return "SUCCESS";
    }

    @GetMapping("/read")
    @ApiOperation(value = "查询城市信息", notes = "查询城市信息")
    public List<TCity> testFind() {
        List<TCity> list = cityMapper.selectList(new QueryWrapper<>());
        list.forEach(city -> {
            System.out.println(city.getId() + " " + city.getName() + " " + city.getProvince());
        });
        return list;
    }
}
