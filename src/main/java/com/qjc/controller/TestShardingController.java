package com.qjc.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qjc.entity.TOrder;
import com.qjc.entity.TOrderExtend;
import com.qjc.entity.TOrderItem;
import com.qjc.mapper.TOrderItemMapper;
import com.qjc.mapper.TOrderMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

/**
 * @ClassName: TestShardingController
 * @Description:
 * @Author: qjc
 * @Date: 2022/4/27 1:08 下午
 */
@RestController
@RequestMapping("/sharding")
@Api(value = "/sharding", tags = "测试分库分表操作")
public class TestShardingController {


    @Autowired
    private TOrderMapper orderMapper;
    @Autowired
    private TOrderItemMapper orderItemMapper;

    private static final Integer num = 10;
    private static final String orderNoPre = "NO-";

    @GetMapping("/order/add")
    @ApiOperation(value = "插入订单信息", notes = "插入订单信息")
    public void testOrderSave() {
        for (int i = 1; i <= num; i++) {
            String orderNo = orderNoPre + String.format("%04d", i);
            TOrder order = new TOrder();
            order.setOrderNo(orderNo);
            order.setCreateName("订单名称" + i);
            order.setPrice(new BigDecimal("" + i));
            orderMapper.insert(order);

            TOrderItem orderItem = new TOrderItem();
            Long orderId = order.getOrderId();
            orderItem.setOrderId(orderId);
            orderItem.setOrderNo(orderNo);
            orderItem.setItemName("商品名称" + i);
            orderItem.setItemDesc("商品描述" + i);
            orderItemMapper.insert(orderItem);
        }
    }

    @GetMapping("/order/query")
    @ApiOperation(value = "查询订单信息", notes = "查询订单信息")
    public void testOrderQuery() {
        List<TOrder> orderListByPage = orderMapper.selectList(new QueryWrapper<>());
        System.err.println(JSON.toJSONString(orderListByPage));
    }

    @GetMapping("/order/query/join")
    @ApiOperation(value = "连表查询订单信息", notes = "连表查询订单信息")
    public void testOrderExtendQuery() {
        List<TOrderExtend> orderListByPage = orderMapper.findOrderListByPage();
        System.err.println(JSON.toJSONString(orderListByPage));
    }

}
