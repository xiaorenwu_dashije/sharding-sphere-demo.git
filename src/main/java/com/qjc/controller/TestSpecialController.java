package com.qjc.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qjc.entity.TUser;
import com.qjc.mapper.TUserMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: TestSpecialController
 * @Description:
 * @Author: qjc
 * @Date: 2022/4/27 1:14 下午
 */
@RestController
@RequestMapping("/special")
@Api(value = "/special", tags = "测试专库专表操作")
public class TestSpecialController {

    @Autowired
    private TUserMapper userMapper;

    @GetMapping("/add")
    @ApiOperation(value = "插入用户信息", notes = "插入用户信息")
    public String testInsertUser(@ApiParam("姓名") @RequestParam("name") String name,
                                 @ApiParam("地址") @RequestParam("address") String address,
                                 @ApiParam("年龄") @RequestParam("age") Integer age) {
        TUser user = new TUser();
        user.setName(name);
        user.setAge(age);
        user.setAddress(address);
        userMapper.insert(user);
        return "SUCCESS";
    }

    @GetMapping("/query")
    @ApiOperation(value = "查询用户信息", notes = "插入用户信息")
    public TUser testQueryUser(@ApiParam("姓名") @RequestParam("name") String name) {
        TUser user = userMapper.selectOne(new LambdaQueryWrapper<TUser>().eq(TUser::getName, name));
        return user;
    }
}
