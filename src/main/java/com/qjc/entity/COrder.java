package com.qjc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("c_order")
public class COrder extends Model {

    @TableId(value = "id")
    private Long id;

    private Boolean isDel;

    private Integer companyId;

    private Long positionId;

    private Integer userId;

    private Integer publishUserId;

    private Integer resumeType;

    private String status;

    private Date createTime;

    private Date updateTime;

}
