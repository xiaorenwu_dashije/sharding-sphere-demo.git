package com.qjc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @ClassName: TCity
 * @Description:
 * @Author: qjc
 * @Date: 2021/11/10 5:24 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_city")
public class TCity extends Model {

    /**
     * 类型必须用包装类型，否则主键生成一直是0
     */
    @TableId(value = "id")
    private Long id;

    private String name;

    private String province;

}
