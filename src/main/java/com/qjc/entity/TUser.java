package com.qjc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @ClassName: TUser
 * @Description:
 * @Author: qjc
 * @Date 2021-03-25 2:52 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_user")
public class TUser extends Model {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id")
    private Long id;

    private String name;

    private String address;

    private Integer age;

//    private String pwd;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
