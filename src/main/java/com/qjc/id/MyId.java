package com.qjc.id;

import org.apache.shardingsphere.core.strategy.keygen.SnowflakeShardingKeyGenerator;
import org.apache.shardingsphere.spi.keygen.ShardingKeyGenerator;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

public class MyId implements ShardingKeyGenerator {

    private SnowflakeShardingKeyGenerator snow = new SnowflakeShardingKeyGenerator();

    private AtomicLong atomic = new AtomicLong(0);

    @Override
    public Comparable<?> generateKey() {
        System.out.println("------执行了自定义主键生成器MyId-------");
//        return snow.generateKey();
        return atomic.incrementAndGet();
    }

    @Override
    public String getType() {
        return "QJCKEY";
    }

    @Override
    public Properties getProperties() {
        return null;
    }

    @Override
    public void setProperties(Properties properties) {

    }
}
