package com.qjc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qjc.entity.COrder;
import com.qjc.entity.TOrder;
import com.qjc.entity.TOrderExtend;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qjc
 * @since 2021-03-25
 */
@Repository
public interface COrderMapper extends BaseMapper<COrder> {
}
