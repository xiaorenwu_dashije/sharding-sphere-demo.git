package com.qjc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qjc.entity.TCity;
import com.qjc.entity.TConfig;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author qjc
 * @since 2021-03-25
 */
@Repository
public interface TCityMapper extends BaseMapper<TCity> {

}
