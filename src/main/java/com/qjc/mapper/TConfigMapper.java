package com.qjc.mapper;

import com.qjc.entity.TConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qjc
 * @since 2021-03-25
 */
@Repository
public interface TConfigMapper extends BaseMapper<TConfig> {

}
