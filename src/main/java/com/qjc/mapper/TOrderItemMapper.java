package com.qjc.mapper;

import com.qjc.entity.TOrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qjc
 * @since 2021-03-26
 */
@Repository
public interface TOrderItemMapper extends BaseMapper<TOrderItem> {

}
