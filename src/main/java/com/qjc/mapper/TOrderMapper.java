package com.qjc.mapper;

import com.qjc.entity.TOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qjc.entity.TOrderExtend;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qjc
 * @since 2021-03-25
 */
@Repository
public interface TOrderMapper extends BaseMapper<TOrder> {
    List<TOrderExtend> findOrderListByPage();
}
