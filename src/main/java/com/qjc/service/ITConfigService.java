package com.qjc.service;

import com.qjc.entity.TConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author qjc
 * @since 2021-03-25
 */
public interface ITConfigService extends IService<TConfig> {

}
