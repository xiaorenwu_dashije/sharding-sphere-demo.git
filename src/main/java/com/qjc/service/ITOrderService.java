package com.qjc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qjc.entity.TOrder;
import com.qjc.entity.TOrderExtend;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author qjc
 * @since 2021-03-25
 */
public interface ITOrderService extends IService<TOrder> {

    List<TOrderExtend> findOrderListByPage();

}
