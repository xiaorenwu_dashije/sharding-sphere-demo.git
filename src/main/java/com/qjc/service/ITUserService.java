package com.qjc.service;

import com.qjc.entity.TUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author qjc
 * @since 2021-03-26
 */
public interface ITUserService extends IService<TUser> {

}
