package com.qjc.service.impl;

import com.qjc.entity.TConfig;
import com.qjc.mapper.TConfigMapper;
import com.qjc.service.ITConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author qjc
 * @since 2021-03-25
 */
@Service
public class TConfigServiceImpl extends ServiceImpl<TConfigMapper, TConfig> implements ITConfigService {

}
