package com.qjc.service.impl;

import com.qjc.entity.TOrderItem;
import com.qjc.mapper.TOrderItemMapper;
import com.qjc.service.ITOrderItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author qjc
 * @since 2021-03-26
 */
@Service
public class TOrderItemServiceImpl extends ServiceImpl<TOrderItemMapper, TOrderItem> implements ITOrderItemService {

}
