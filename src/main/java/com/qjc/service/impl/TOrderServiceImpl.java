package com.qjc.service.impl;

import com.qjc.entity.TOrder;
import com.qjc.entity.TOrderExtend;
import com.qjc.mapper.TOrderMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qjc.service.ITOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author qjc
 * @since 2021-03-25
 */
@Service
public class TOrderServiceImpl extends ServiceImpl<TOrderMapper, TOrder> implements ITOrderService {

    @Autowired
    private TOrderMapper orderMapper;

    @Override
    public List<TOrderExtend> findOrderListByPage() {
        return orderMapper.findOrderListByPage();
    }
}
