package com.qjc.service.impl;

import com.qjc.entity.TUser;
import com.qjc.mapper.TUserMapper;
import com.qjc.service.ITUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author qjc
 * @since 2021-03-26
 */
@Service
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements ITUserService {

}
