package com.qjc;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qjc.entity.*;
import com.qjc.mapper.TConfigMapper;
import com.qjc.mapper.TOrderItemMapper;
import com.qjc.mapper.TOrderMapper;
import com.qjc.mapper.TUserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShardingSphereDemoTest {

    @Autowired
    private TOrderMapper orderMapper;
    @Autowired
    private TOrderItemMapper orderItemMapper;
    @Autowired
    private TUserMapper userMapper;
    @Autowired
    private TConfigMapper configMapper;

    private static final Integer num = 1000;
    private static final String orderNoPre = "NO-";

    /**
     * 测试分库分表-插入
     */
    @Test
    public void testOrderSave() {
        for (int i = 1; i <= num; i++) {
            String orderNo = orderNoPre + String.format("%04d", i);
            TOrder order = new TOrder();
            order.setOrderNo(orderNo);
            order.setCreateName("订单名称" + i);
            order.setPrice(new BigDecimal("" + i));
            orderMapper.insert(order);

            TOrderItem orderItem = new TOrderItem();
            Long orderId = order.getOrderId();
            orderItem.setOrderId(orderId);
            orderItem.setOrderNo(orderNo);
            orderItem.setItemName("商品名称" + i);
            orderItem.setItemDesc("商品描述" + i);
            orderItemMapper.insert(orderItem);
        }
    }

    /**
     * 测试分库分表-查询
     */
    @Test
    public void testOrderExtendQuery() {
        List<TOrderExtend> orderListByPage = orderMapper.findOrderListByPage();
        System.err.println(JSON.toJSONString(orderListByPage));
    }

    @Test
    public void testOrderQuery() {
        List<TOrder> orderListByPage = orderMapper.selectList(new QueryWrapper<>());
        System.err.println(JSON.toJSONString(orderListByPage));
    }

    /**
     * 测试专库专表
     */
    @Test
    public void testInsertUser() {
        TUser user = new TUser();
        user.setName("用户");
        user.setAge(18);
        user.setAddress("地址");
        userMapper.insert(user);
    }

    @Test
    public void testQueryUseer() {
        TUser user1 = userMapper.selectById(1376363351721992194L);
        System.err.println(user1);
    }

    /**
     * 测试公共表
     */
    @Test
    public void testConfig() {
        TConfig config = new TConfig();
        config.setRemark("bbbb");
        configMapper.insert(config);
    }

}
