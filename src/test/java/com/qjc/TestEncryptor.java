//package com.qjc;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.qjc.entity.TUser;
//import com.qjc.mapper.TUserMapper;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.annotation.Repeat;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import javax.annotation.Resource;
//import java.util.List;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = ShardingSphereDemoApplication.class)
//public class TestEncryptor {
//
//    @Resource
//    private TUserMapper userMapper;
//
//    @Test
//    @Repeat(1)  // 执行次数
//    public void testAdd() {
//        TUser user = new TUser();
//        user.setName("tiger");
//        user.setAddress("北京");
//        user.setAge(18);
//        user.setPwd("abc");
//        userMapper.insert(user);
//    }
//
//    @Test
//    public void testFind() {
//        QueryWrapper<TUser> queryWrapper = new QueryWrapper();
//        queryWrapper
//                .lambda()
//                .eq(TUser::getPwd, "abc")
//        ;
//        List<TUser> list = userMapper.selectList(queryWrapper);
//        list.forEach(user -> {
//            System.out.println(user.getId() + " " + user.getName() + " " + user.getPwd());
//        });
//    }
//
//}
