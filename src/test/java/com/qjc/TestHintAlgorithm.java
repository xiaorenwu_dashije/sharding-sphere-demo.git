package com.qjc;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qjc.entity.TCity;
import com.qjc.entity.TOrder;
import com.qjc.mapper.TCityMapper;
import com.qjc.mapper.TOrderMapper;
import org.apache.shardingsphere.api.hint.HintManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingSphereDemoApplication.class)
public class TestHintAlgorithm {

    @Resource
    private TCityMapper cityMapper;
    @Autowired
    private TOrderMapper orderMapper;

    @Test
    public void testAdd() {
        HintManager hintManager = HintManager.getInstance();
        // 通过MyHintShardingAlgorithm可知，强制路由到ds-${value%2}
        hintManager.setDatabaseShardingValue(1L);
        for (int i = 0; i < 10; i++) {
            TCity city = new TCity();
            city.setName("商丘市" + i);
            city.setProvince("河南省" + i);
            cityMapper.insert(city);
        }
    }

    @Test
    public void test1() {
        HintManager hintManager = HintManager.getInstance();
        // 通过MyHintShardingAlgorithm可知，强制路由到ds-${value%2}
//        hintManager.setDatabaseShardingValue(1L);
        hintManager.addDatabaseShardingValue("t_city", 1L);
        List<TCity> list = cityMapper.selectList(new QueryWrapper<>());
        list.forEach(city -> {
            System.out.println(city.getId() + " " + city.getName() + " " + city.getProvince());
        });
    }

    @Test
    public void test2() {
        HintManager hintManager = HintManager.getInstance();
        // 强制路由到库ds-1
        hintManager.addDatabaseShardingValue("t_order", 1L);
        // 强制路由到表t_order_1
        hintManager.addTableShardingValue("t_order", 1L);
        List<TOrder> list = orderMapper.selectList(new QueryWrapper<>());
        list.forEach(order -> {
            System.out.println(order.getOrderId() + " " + order.getOrderNo());
        });
    }

}
