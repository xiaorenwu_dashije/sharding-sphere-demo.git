package com.qjc;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qjc.entity.TCity;
import com.qjc.mapper.TCityMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingSphereDemoApplication.class)
public class TestMasterSlave {

    @Resource
    private TCityMapper cityMapper;

    /**
     * 测试读写分离
     */
    @Test
    public void testAdd() {
        TCity city = new TCity();
        city.setName("shanghai");
        city.setProvince("shanghai");
        cityMapper.insert(city);
    }

    @Test
    public void testFind() {
        List<TCity> list = cityMapper.selectList(new QueryWrapper<>());
        list.forEach(city -> {
            System.out.println(city.getId() + " " + city.getName() + " " + city.getProvince());
        });
    }

}
