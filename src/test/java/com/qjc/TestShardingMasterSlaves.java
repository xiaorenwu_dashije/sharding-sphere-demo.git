package com.qjc;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qjc.entity.COrder;
import com.qjc.entity.TCity;
import com.qjc.mapper.COrderMapper;
import com.qjc.mapper.TCityMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingSphereDemoApplication.class)
public class TestShardingMasterSlaves {

    @Resource
    private COrderMapper orderMapper;

    @Test
    @Repeat(10)
    public void testShardingCOrder() {
        Random random = new Random();
        int companyId = random.nextInt(10);
        COrder order = new COrder();
        order.setIsDel(false);
        order.setCompanyId(companyId);
        order.setPositionId(3242342L);
        order.setUserId(2222);
        order.setPublishUserId(1111);
        order.setResumeType(1);
        order.setStatus("AUTO");
        order.setCreateTime(new Date());
        order.setUpdateTime(new Date());
        orderMapper.insert(order);
    }

    @Test
    public void testFind() {
        List<COrder> list = orderMapper.selectList(new QueryWrapper<>());
        list.forEach(city -> {
            System.out.println(city.getId() + " " + city.getCompanyId());
        });
    }

}
